package com.example.demo;

import java.util.ArrayList;
import java.util.List;
import org.springframework.boot.*;
import org.springframework.boot.autoconfigure.*;
import org.springframework.web.bind.annotation.*;

@SpringBootApplication
@RestController
public class DemoApplication {

    @GetMapping("/")
    String home() {
        return "Spring is here!";
    }

    @GetMapping("/students/{id}")
    String students(@PathVariable("id") int index) {
        List<String> students = new ArrayList<>();
        students.add("Julian");
        students.add("Juan Diega");
        students.add("Julian");
        students.add("Julian");
        students.add("christian");
        return students.get(index);

    }

    public static void main(String[] args) {
        SpringApplication.run(DemoApplication.class, args);
    }
}
